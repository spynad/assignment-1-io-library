; vi:ft=nasm
section .text

exit:
; rdi - exit code
    mov rax, 60
    syscall

string_length:
; rdi - pointer to a null-terminated string
; returns
; rax - string length
    xor rax, rax
.strlen_loop:
    cmp byte [rdi+rax], 0
    je .strlen_end
    inc rax
    jmp .strlen_loop
.strlen_end:
    ret

print_string:
; rdi - pointer to a null-terminated string
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

print_newline:
    push 10
    pop rdi

print_char:
; rdi - char
    xor rax, rax
    push di
    mov rdi, rsp
    call print_string
    pop di
    ret

print_uint:
; rdi - uint
    xor rax, rax
    mov rax, rdi
    mov rdi, rsp
    push 0
    sub rsp, 16

    dec rdi
    push rbx
    mov rbx, 10
.loop:
    xor rdx, rdx
    div rbx
    or rdx, 0x30
    dec rdi
    mov [rdi], dl
    test rax, rax
    jnz .loop

    call print_string
    pop rbx
    add rsp, 24
    ret

print_int:
; rdi - int
    xor rax, rax
    test rdi, rdi
    jns not_neg
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    call print_uint
    ret
not_neg:
    call print_uint
    ret

string_equals:
; rdi - pointer to the first string
; rsi - pointer to the second string
; returns
; rax - 1 if strings are equal otherwise 0
    xor rax, rax
    xor rdx, rdx
.loop:
    mov al, byte [rdi]
    mov dl, byte [rsi]
    inc rdi
    inc rsi
    cmp al, dl
    je .test_nullterm
    mov rax, 0
    ret

.test_nullterm:
    cmp al, 0
    jne .loop
    mov rax, 1
    ret

read_char:
    xor rax, rax
    xor rdi, rdi
    push 0;
    mov rsi, rsp
    mov rdx, 1
    syscall

    pop rax
    ret 

read_word:
; rdi - pointer to a buffer
; rsi - buffer size
    push r12
    xor r12, r12
	push rdi
    push rsi

.first_symbol:
    call read_char
    cmp al, 10
    je .first_symbol
    cmp al, 9
    je .first_symbol
    cmp al, 0x20
    je .first_symbol
    test al, al
	pop rsi
    pop rdi
    jz .exit_nullterm

.place_symbol:
	cmp r12, rsi ; check if buffer length is smaller then word length
    ja .word_too_long
    mov byte [rdi + r12], al
    inc r12
	
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    cmp al, 10
    je .exit_nullterm
    cmp al, 9
    je .exit_nullterm
    cmp al, 0x20
    je .exit_nullterm
    test al, al
    jz .exit_nullterm
    jmp .place_symbol

.exit_nullterm:
	cmp r12, rsi
    ja .word_too_long
    mov byte [rdi + r12], 0
    mov rax, rdi
    mov rdx, r12
    pop r12
    ret

.word_too_long:
    xor rax, rax
    xor rdx, rdx
    pop r12
    ret


parse_uint:
; rdi  - pointer to a string
; returns 
; rax - uint
; rdx - uint length
    xor rax, rax
    xor rcx, rcx
    push rbx
    mov r8, 10
.loop:
    mov bl, byte [rdi]
    test bl, bl
    jz .finish
    cmp bl, 0x30
    jb .finish
    cmp bl, 0x39
    ja .finish
    mul r8
    and bl, 0xF
    add rax, rbx
    inc rdi
    inc rcx
    jmp .loop
.finish:
    pop rbx
    mov rdx, rcx
    ret

parse_int:
; rdi - pointer to a string
; returns 
; rax - int
; rdx - int length
    xor rax, rax
    push rbx
    xor rbx, rbx
    mov bl, byte [rdi]
    cmp bl, '-'
    je .neg_num
    call parse_uint
    pop rbx
    ret
.neg_num:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    pop rbx
    ret

string_copy:
    ;rdi - pointer to a string
    ;rsi - pointer to a buffer
    ;rdx - buffer length
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jae .small_buff
    push rsi
.strcpy_loop: ;we are copying bytes from dest string to new string until we meet null-terminator
    mov dl, byte[rdi]
    mov byte[rsi], dl
    inc rdi
    inc rsi
    test dl, dl
    jnz .strcpy_loop
    pop rax
    ret
.small_buff:
    mov rax, 0
    ret

